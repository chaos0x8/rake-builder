Gem::Specification.new { |s|
  s.name        = 'rake-builder'
  s.version     = '0.5.0'
  s.date        = '2020-02-18'
  s.summary     = "#{s.name} library"
  s.description = "Library for easier rakefile creation"
  s.authors     = ["chaos0x8"]
  s.files       = Dir['lib/**/*.rb']
}
